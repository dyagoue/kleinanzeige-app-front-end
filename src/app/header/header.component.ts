import { Component, OnInit } from '@angular/core';
import {BenutzersService} from '../services/benutzers.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  constructor(public benutzerService: BenutzersService) { }

  ngOnInit() {
  }

  onSignOut() {
    this.benutzerService.logout();
  }
}
