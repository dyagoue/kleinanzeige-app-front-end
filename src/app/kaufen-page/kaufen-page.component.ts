import { Component, OnInit } from '@angular/core';
import {BenutzersService} from '../services/benutzers.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AnzeigesService} from '../services/anzeiges.service';

@Component({
  selector: 'app-kaufen-page',
  templateUrl: './kaufen-page.component.html',
  styleUrls: ['./kaufen-page.component.scss']
})
export class KaufenPageComponent implements OnInit {

  benutzername;
  id;
  result: string;

  constructor(private benutzerService: BenutzersService,
              private anzeigeService: AnzeigesService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.benutzername = this.benutzerService.getUsername();
    this.anzeigeService.kaufAnzeige(this.id, this.benutzername).subscribe(
      data => {
        if (data && data.titel.length > 0) {
          this.result = 'Anzeige wurde erfolgreich gekauft !';
        } else {
          this.result = 'Anzeige wurde nicht gekauft !';
        }
      }
    );
  }

}
