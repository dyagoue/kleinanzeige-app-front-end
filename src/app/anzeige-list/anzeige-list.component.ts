import { Component, OnInit } from '@angular/core';
import {AnzeigesService} from '../services/anzeiges.service';
import {error} from '@angular/compiler/src/util';
import {Timestamp} from 'rxjs';
import {BenutzersService} from '../services/benutzers.service';

@Component({
  selector: 'app-anzeige-list',
  templateUrl: './anzeige-list.component.html',
  styleUrls: ['./anzeige-list.component.scss']
})
export class AnzeigeListComponent implements OnInit {

  anzeigeList: any[];

  constructor(private anzeigesService: AnzeigesService,
              private benutzerService: BenutzersService) { }

  ngOnInit() {
    this.anzeigesService.getAllAnzeige().subscribe(
      data => {
        this.anzeigeList = data;
      }, error => console.log(error)
    );
  }

  public createDate(date: string) {
    return new Date(date.toString().substr(0, date.length - 5));
  }
}
