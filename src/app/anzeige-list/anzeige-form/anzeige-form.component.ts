import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {AnzeigesService} from '../../services/anzeiges.service';
import {error} from '@angular/compiler/src/util';
import {BenutzersService} from '../../services/benutzers.service';

@Component({
  selector: 'app-anzeige-form',
  templateUrl: './anzeige-form.component.html',
  styleUrls: ['./anzeige-form.component.scss']
})
export class AnzeigeFormComponent implements OnInit {

  createAngeigeForm: FormGroup;
  ersteller: string;

  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private authService: AuthService,
              private anzeigeService: AnzeigesService,
              private benutzerService: BenutzersService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.createAngeigeForm = this.formBuilder.group({
      titel: ['', Validators.required],
      text: ['', Validators.required],
      preis: ['0', Validators.required],
      kategorie: ['Multimedia & Elektronik', Validators.required]
    });
  }

  onSubmit() {
    const titel = this.createAngeigeForm.get('titel').value;
    const text = this.createAngeigeForm.get('text').value;
    const preis = this.createAngeigeForm.get('preis').value;
    const kategorie = this.createAngeigeForm.get('kategorie').value;
    this.ersteller = this.benutzerService.getUsername();
    this.anzeigeService.addAnzeige(titel, text, preis, this.ersteller, kategorie).subscribe(
      data => {
        if (data && data.titel.length > 0) {
          this.router.navigate(['/anzeige/anzeiges']);
        } else { console.log('erreur'); }
      }, error => console.log(error)
    );
  }

}
