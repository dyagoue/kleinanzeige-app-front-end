import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Benutzer} from '../models/Benutzer.models';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BenutzersService {

  constructor(private httpClient: HttpClient,
              private router: Router) { }

  login(benutzername: string, password: string): Observable<any> {
    // return this.httpClient.post('http://localhost:8080/webapi/benutzer/login', benutzer);
    // return this.httpClient.get('http://localhost:8080/webapi/benutzer/login' + '/' + benutzername + '/' + password).toPromise();
    return this.httpClient.get('http://localhost:8080/webapi/benutzer/login' + '/' + benutzername + '/' + password);
  }

  signup(benutzername: string, name: string, password: string): Observable<any> {
    return this.httpClient.get('http://localhost:8080/webapi/benutzer/addbenutzer' + '/' + benutzername + '/' + name + '/' + password);
  }

  getBenutzerAnzeige(benutzername: string): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/benutzer/getBenutzerAnzeige/${benutzername}/${this.getToken()}`);
    // return this.httpClient.get('http://localhost:8080/webapi/benutzer/getBenutzerAnzeige' + '/' + benutzername);
  }
  public setUser(data: {benutzername: string , token: string}) {
    localStorage.setItem('session', JSON.stringify(data));
  }
  public isAuth() {
    const data = localStorage.getItem('session') ? JSON.parse(localStorage.getItem('session')) : null;
    return data !== null;
  }
  public getUsername() {
    if (this.isAuth()) {
      return JSON.parse(localStorage.getItem('session')).benutzername;
    }
  }
  public getToken() {
    if (this.isAuth()) {
      return JSON.parse(localStorage.getItem('session')).token;
    }
  }
  public logout() {
    localStorage.clear();
    this.router.navigate(['']);
  }

  getBenutzerByBenutzername(benutzername: string): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/benutzer/getBenutzerByBenutzername/${benutzername}/${this.getToken()}`);
  }

  searchByTitel(titel: string): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/anzeige/searchbytitel/${titel}`);
  }
}
