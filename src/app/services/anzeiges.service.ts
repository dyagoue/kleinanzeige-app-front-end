import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BenutzersService} from './benutzers.service';

@Injectable({
  providedIn: 'root'
})
export class AnzeigesService {

  constructor(private httpClient: HttpClient,
              private benutzerService: BenutzersService) { }

  getAllAnzeige(): Observable<any> {
    return this.httpClient.get('http://localhost:8080/webapi/anzeige/anzeiges');
  }

  addAnzeige(titel: string, text: string, preis: number, ersteller: string, kategorie: string): Observable<any> {
    return this.httpClient.get('http://localhost:8080/webapi/anzeige/addanzeige' + '/' + titel + '/' + text + '/' + preis + '/' + ersteller + '/' + kategorie);
  }

  kaufAnzeige(id: number, benutzername: string): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/anzeige/kaufen/${id}/${benutzername}`);
  }

  getAnzeigeById(id: number): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/anzeige/getAnzeigeById/${id}`);
  }

  getAnzeigeKommentare(id: number): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/anzeige/getAnzeigeKommentare/${id}`);
  }

  addKommentar(id: number, benutzername: string, text: string): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/kommentar/add/${id}/${benutzername}/${text}`);
  }

  deleteAnzeigeById(id: number): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/anzeige/deletebyid/${id}`);
  }

  updateAnzeige(id: number, titel: string, text: string, preis: number, kategorie: string): Observable<any> {
    return this.httpClient.get(`http://localhost:8080/webapi/anzeige/update/${id}/${titel}/${text}/${preis}/${kategorie}`);
  }
}
