import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {AnzeigeListComponent} from './anzeige-list/anzeige-list.component';
import {SigninComponent} from './auth/signin/signin.component';
import {SignupComponent} from './auth/signup/signup.component';
import {AnzeigeFormComponent} from './anzeige-list/anzeige-form/anzeige-form.component';
import {BenutzerProfilComponent} from './benutzer-profil/benutzer-profil.component';
import {WelcomePageComponent} from './welcome-page/welcome-page.component';
import {SearchResultComponent} from './search-result/search-result.component';
import {KaufenPageComponent} from './kaufen-page/kaufen-page.component';
import {KommentarPageComponent} from './kommentar-page/kommentar-page.component';
import {DeletePageComponent} from './delete-page/delete-page.component';
import {UpdateAnzeigeComponent} from './update-anzeige/update-anzeige.component';
import {AuthGuardService} from './services/auth-guard.service';

const routes: Routes = [
  { path: 'anzeige/anzeiges', component: AnzeigeListComponent },
  { path: 'anzeige/search', component: SearchResultComponent },
  { path: 'anzeige/kaufen/:id', canActivate: [AuthGuardService], component: KaufenPageComponent },
  { path: 'anzeige/delete/:id', canActivate: [AuthGuardService], component: DeletePageComponent },
  { path: 'anzeige/update/:id', canActivate: [AuthGuardService], component: UpdateAnzeigeComponent },
  { path: 'anzeige/kommentieren/:id', canActivate: [AuthGuardService], component: KommentarPageComponent },
  { path: 'anzeige/anzeiges/addanzeige', canActivate: [AuthGuardService], component: AnzeigeFormComponent },
  { path: 'benutzerprofil/:benutzername', component: BenutzerProfilComponent },
  { path: 'auth/signin', component: SigninComponent},
  { path: 'auth/signup', component: SignupComponent},
  { path: '', component: WelcomePageComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
