import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { AnzeigeListComponent } from './anzeige-list/anzeige-list.component';
import { SingleAnzeigeComponent } from './anzeige-list/single-anzeige/single-anzeige.component';
import { AnzeigeFormComponent } from './anzeige-list/anzeige-form/anzeige-form.component';
import { HeaderComponent } from './header/header.component';
import {AuthService} from './services/auth.service';
import {AnzeigesService} from './services/anzeiges.service';
import {AuthGuardService} from './services/auth-guard.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BenutzersService} from './services/benutzers.service';
import { BenutzerProfilComponent } from './benutzer-profil/benutzer-profil.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { KaufenPageComponent } from './kaufen-page/kaufen-page.component';
import { KommentarPageComponent } from './kommentar-page/kommentar-page.component';
import { DeletePageComponent } from './delete-page/delete-page.component';
import { UpdateAnzeigeComponent } from './update-anzeige/update-anzeige.component';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    SigninComponent,
    AnzeigeListComponent,
    SingleAnzeigeComponent,
    AnzeigeFormComponent,
    HeaderComponent,
    BenutzerProfilComponent,
    WelcomePageComponent,
    SearchResultComponent,
    KaufenPageComponent,
    KommentarPageComponent,
    DeletePageComponent,
    UpdateAnzeigeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    AuthService,
    AnzeigesService,
    BenutzersService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
