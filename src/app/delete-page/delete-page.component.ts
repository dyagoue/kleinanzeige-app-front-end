import { Component, OnInit } from '@angular/core';
import {BenutzersService} from '../services/benutzers.service';
import {AnzeigesService} from '../services/anzeiges.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-delete-page',
  templateUrl: './delete-page.component.html',
  styleUrls: ['./delete-page.component.scss']
})
export class DeletePageComponent implements OnInit {

  id;
  result: string;

  constructor(private benutzerService: BenutzersService,
              private anzeigeService: AnzeigesService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.anzeigeService.deleteAnzeigeById(this.id).subscribe(
      data => {
        if (data && data.titel.length > 0) {
          this.result = 'Anzeige wurde erfolgreich gelöscht !';
        } else {
          this.result = 'Anzeige wurde nicht gelöscht !';
        }
      }
    );
  }

}
