import { Component, OnInit } from '@angular/core';
import {BenutzersService} from '../services/benutzers.service';
import {AnzeigesService} from '../services/anzeiges.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {error} from '@angular/compiler/src/util';
import {Kommentar} from '../models/Kommentar.model';

@Component({
  selector: 'app-kommentar-page',
  templateUrl: './kommentar-page.component.html',
  styleUrls: ['./kommentar-page.component.scss']
})
export class KommentarPageComponent implements OnInit {

  kommentarList: Kommentar[];
  anzeige: any;
  id;
  kommentForm: FormGroup;

  constructor(private benutzerService: BenutzersService,
              private formBuilder: FormBuilder,
              private anzeigeService: AnzeigesService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getKommentare();
    this.kommentForm = this.formBuilder.group({
      text: ['', Validators.required]
    })
    this.anzeigeService.getAnzeigeById(this.id).subscribe(
      data => {
        this.anzeige = data;
      }, error => console.log(error)
    );
  }

  getKommentare() {
    this.anzeigeService.getAnzeigeKommentare(this.id).subscribe(
      data => {
        this.kommentarList = data;
      }, error => console.log(error)
    );
  }

  onSubmit() {
    const text = this.kommentForm.get('text').value;
    this.anzeigeService.addKommentar(this.id, this.benutzerService.getUsername(), text).subscribe(
      data => {
        if (data && data.text.length > 0) {
          window.location.reload();
        }
      }, error => console.log(error)
    );
  }

  public createDate(date: string) {
    return new Date(date.toString().substr(0, date.length - 5));
  }

}
