import { Component, OnInit } from '@angular/core';
import {HeaderComponent} from '../header/header.component';
import {BenutzersService} from '../services/benutzers.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  searchForm: FormGroup;
  searchList: any;

  constructor(private formBuilder: FormBuilder,
              public benutzerService: BenutzersService,
              private router: Router) { }
  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.searchForm = this.formBuilder.group( {
      titel: ['']
    });
  }

  onSubmit() {
    const titel = this.searchForm.get('titel').value;
    this.benutzerService.searchByTitel(titel).subscribe(
      data => {
        this.searchList = data;
        this.router.navigate(['anzeige/search']);
      }, error => console.log(error)
    );
  }

  public createDate(date: string) {
    return new Date(date.toString().substr(0, date.length - 5));
  }
}
