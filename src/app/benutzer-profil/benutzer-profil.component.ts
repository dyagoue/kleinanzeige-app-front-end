import { Component, OnInit } from '@angular/core';
import {BenutzersService} from '../services/benutzers.service';
import {Anzeige} from '../models/Anzeige.model';
import {ActivatedRoute} from '@angular/router';
import {Benutzer} from '../models/Benutzer.models';

@Component({
  selector: 'app-benutzer-profil',
  templateUrl: './benutzer-profil.component.html',
  styleUrls: ['./benutzer-profil.component.scss']
})
export class BenutzerProfilComponent implements OnInit {

  benutzerAnzeigeList: any[];
  benutzer: Benutzer;
  benutzername;

  constructor(private benutzerService: BenutzersService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.benutzername = this.route.snapshot.paramMap.get('benutzername');
    this.getBenutzerData();
    this.benutzerService.getBenutzerAnzeige(this.benutzername).subscribe(
      data => {
        this.benutzerAnzeigeList = data;
      }, error => console.log(error)
    );
  }

  public getBenutzerData() {
    this.benutzerService.getBenutzerByBenutzername(this.benutzername).subscribe(
      data => {
        this.benutzer = data;
      }, error => console.log(error)
    );
  }

  public createDate(date: string) {
    return new Date(date.toString().substr(0, date.length - 5));
  }

}
