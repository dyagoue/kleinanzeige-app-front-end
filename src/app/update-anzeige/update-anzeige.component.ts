import { Component, OnInit } from '@angular/core';
import {BenutzersService} from '../services/benutzers.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AnzeigesService} from '../services/anzeiges.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-update-anzeige',
  templateUrl: './update-anzeige.component.html',
  styleUrls: ['./update-anzeige.component.scss']
})
export class UpdateAnzeigeComponent implements OnInit {

  updateAnzeigeForm: FormGroup;
  anzeige: any;
  titel: string
  id;

  constructor(private benutzerService: BenutzersService,
              private formBuilder: FormBuilder,
              private anzeigeService: AnzeigesService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.anzeigeService.getAnzeigeById(this.id).subscribe(
      data => {
        this.anzeige = data;
        this.updateAnzeigeForm.controls.titel.patchValue(this.anzeige.titel);
        this.updateAnzeigeForm.controls.text.patchValue(this.anzeige.text);
        this.updateAnzeigeForm.controls.preis.patchValue(this.anzeige.preis);
        this.updateAnzeigeForm.controls.kategorie.patchValue(this.anzeige.kategorie);
      }, error => console.log(error)
    );

    this.updateAnzeigeForm = this.formBuilder.group({
      titel: ['', Validators.required],
      text: ['', Validators.required],
      preis: ['', Validators.required],
      kategorie: ['', Validators.required]
    });
  }

  onSubmit() {
    const titel = this.updateAnzeigeForm.get('titel').value;
    const text = this.updateAnzeigeForm.get('text').value;
    const preis = this.updateAnzeigeForm.get('preis').value;
    const kategorie = this.updateAnzeigeForm.get('kategorie').value;
    this.anzeigeService.updateAnzeige(this.id, titel, text, preis, kategorie).subscribe(
      data => {
        if (data && data.titel.length > 0) {
          this.router.navigate(['/anzeige/anzeiges']);
        } else { console.log('erreur'); }
      }, error => console.log(error)
    );
  }

}
