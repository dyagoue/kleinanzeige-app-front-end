export interface Anzeige {
  id?: any;
  titel?: string;
  text: any;
  preis?: any;
  erstellungsdatum: any;
  status?: string;
  ersteller?: string;
}
