import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {Benutzer} from '../../models/Benutzer.models';
import {BenutzersService} from '../../services/benutzers.service';
import {error} from '@angular/compiler/src/util';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  signInForm: FormGroup;
  errorMessage: string;
  benutzer: Benutzer;
  rep: any;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private benutzerService: BenutzersService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signInForm = this.formBuilder.group({
      benutzername: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit() {
    const benutzername = this.signInForm.get('benutzername').value;
    const password = this.signInForm.get('password').value;
    // this.benutzer = new Benutzer(benutzername, password);
    // this.benutzerService.login(benutzername, password)
    //   .then((response) => console.log('Hello' + response))
    //   .catch((error) => console.log(error)) ;
    this.benutzerService.login(benutzername, password).subscribe(
      data => {
        // console.log(data);
        // this.rep = data;
        if (data && data.benutzername.length > 0) {
          this.benutzerService.setUser(data);
          this.router.navigate(['/anzeige/anzeiges']);
        } else { console.log('erreur'); }
      }, error => console.log()
    );
  }

}
