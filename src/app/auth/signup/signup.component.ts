import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Benutzer} from '../../models/Benutzer.models';
import {AuthService} from '../../services/auth.service';
import {BenutzersService} from '../../services/benutzers.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signUpForm: FormGroup;
  errorMessage: string;
  benutzer: Benutzer;
  rep: any;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              private benutzerService: BenutzersService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signUpForm = this.formBuilder.group({
      benutzername: ['', [Validators.required]],
      name: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit() {
    const benutzername = this.signUpForm.get('benutzername').value;
    const name = this.signUpForm.get('name').value;
    const password = this.signUpForm.get('password').value;
    // this.benutzer = new Benutzer(benutzername, name, password);
    this.benutzerService.signup(benutzername, name, password).subscribe(
      data => {
        if (data && data.benutzername.length > 0) {
          this.router.navigate(['auth/signin']);
        } else { console.log('erreur'); }
      }, error => console.log()
    );
  }
}
